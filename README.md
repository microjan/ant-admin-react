# 项目技术方案
## 1.1 工具选择
| **类别** | **选择** |
| --- | --- |
| 框架 | [react](https://reactjs.org/) |
| JS 语言 | [TypeScript](http://www.typescriptlang.org/) |
| CSS 语言 | [css-modules](https://github.com/css-modules/css-modules) + [less](http://lesscss.org/) + [postcss](https://github.com/postcss/postcss) |
| JS 编译 | [babel](https://www.babeljs.cn/) |
| 模块打包 | [webpack 全家桶](https://webpack.github.io/) |
| 单元测试 | [jest](https://github.com/facebook/jest) + [enzyme](https://github.com/airbnb/enzyme) + [puppteer](https://github.com/puppeteer/puppeteer) + [jsdom](https://github.com/jsdom/jsdom) |
| 路由 | [react-router](https://github.com/ReactTraining/react-router) |
| 数据流 | [dva](https://dvajs.com/) + [redux 生态](https://www.redux.org.cn/) |
| 代码风格 | [eslint](https://eslint.org/) + [prettier](https://prettier.io/) |
| JS 压缩 | [TerserJS](https://github.com/terser/terser) |
| CSS 压缩 | [cssnano](https://github.com/cssnano/cssnano) |
| 请求库 | [umi-request](https://github.com/umijs/umi-request#readme) |
| UI | [AntDesign](https://ant.design/docs/react/introduce-cn) + [AntDesignPro](https://pro.ant.design/index-cn) |
| 国际化 | [react-intl](https://github.com/formatjs/react-intl) |
| hooks 库 | [umi-hooks](https://hooks.umijs.org/) |
| 静态文档 | [docz](https://www.docz.site/) |
| 微前端 | [qiankun](https://github.com/umijs/qiankun) |
| 图表库 | [antv](https://antv.vision/) |

## 1.2 技术栈选型
### 1.2.1 固定化

- React 框架
- TypeScript 语言
- Less+CSS Modules
- Eslint+Prettier+固定配置
- 固定数据流方案 dva
- 固定 babel 插件
- Jest+Enzyme
- 框架版本不允许锁定，^前缀必须有
- 主要依赖不允许自定义依赖版本
### 1.2.2 配置化

- 不仅是框架功能，还有 UI 界面
- 路由、布局、菜单、导航、tab页签、权限、请求、埋点、错误处理
- 只管写 Page 页面就可以了
#### 1.2.2.1 编译态配置

- 给 node.js 使用，比如 webpack、babel 相关配置，静态路由配置
#### 1.2.2.2 运行态配置

- 给浏览器用、比如渲染逻辑、动态修改路由、获取用户信息
### 1.3 约定化

- 国际化
- 数据流
- MOCK
- 目录结构
- 404
- 权限策略
- Service
- 配置文件
### 1.4 理念

- 通过最佳实践减少不必要的选择的差异
- 通过插件和插件集的架构方式，满足不同场景的业务
- 通过资产市场和场景市场着力解决 70%的开发者问题
- 通过对垂直场景采取强约束的方式，进一步提升研发效率
- 不给选择、配置化、约定化
## 2.Ant Design Pro

- Ant Design Pro 是一个企业级中后台前端/设计解决方案，我们秉承 Ant Design 的设计价值观，致力于在设计规范和基础组件的基础上，继续向上构建，提炼出典型模板/业务组件/配套设计资源，进一步提升企业级中后台产品设计研发过程中的『用户』和『设计者』的体验。
- [pro.ant.design](https://pro.ant.design/)
- [procomponents.ant.design](https://procomponents.ant.design/components/table/)
- [getting-started-cn](https://beta-pro.ant.design/docs/getting-started-cn)
## 2.1 启动项目
### 2.1.1 安装

```shell
git clone https://gitee.com/microjan/ant-admin-react.git

cd ant-admin-react

npm install 

npm start:dev 
```
### 2.1.2 目录结构

- 整个项目的目录结构。
```
├─config # umi 配置，包含路由，构建等配置
├─mock   # 本地模拟数据
├─public
│  └─icons
├─src
│  ├─components # 业务通用组件
│  │  ├─Footer
│  │  ├─HeaderDropdown
│  │  ├─HeaderSearch
│  │  ├─NoticeIcon
│  │  └─RightContent
│  ├─e2e       # 集成测试用例
│  ├─locales   # 国际化资源
│  │  ├─en-US
│  │  ├─id-ID
│  │  ├─pt-BR
│  │  ├─zh-CN
│  │  └─zh-TW
│  ├─pages    # 业务页面入口和常用模板
│  │  ├─ListTableList
│  │  │  └─components
│  │  └─user
│  │      └─login
│  ├─services # 后台接口服务
│  └─utils    # 工具库
└─tests       # 测试工具
```

### 2.1.3功能

```
- 登录 / 注销 / 注册

- 权限验证
  - 页面权限
  - 按钮操作
  - 权限配置

- 全局功能
  - 国际化多语言
  - 动态顶级菜单（支持设置是否启用）
  - 动态侧边栏（支持多级路由嵌套）
  - 动态面包屑（支持自定义配置）
  - Svg Sprite 图标
  - Mock 数据

- 综合实例
  - 引导页
  - 组件示例
    - 编辑器
      - CKEditor
      - tui-editor
    - 图标
      - IconSvg
      - IconFont
      - IconAntd
  - 页面示例
    - 列表页面
      - 标准列表
      - 表格列表
      - 高度自适应表格
      - 搜索列表
    - 表单页面
      - 基础表单
      - 高级表单      
    - 详情页面
      - 基础详情
      - 模块详情
      - 表格详情
  - 权限验证
```

###  2.1.4 Ant tree shaking

- [babel-plugin-import](https://github.com/ant-design/babel-plugin-import#readme) 是用来按需加载 antd 的脚本和样式的插件

``` shell
cnpm i babel-plugin-import -D
```

- 编辑 .webpackrc，使 babel-plugin-import 插件生效。

```
{
  "extraBabelPlugins": [
    ["import", { "libraryName": "antd", "libraryDirectory": "es", "style": "css" }]
  ]
}
```
